const Product = require("../models/Product");
const brcrypt = require("bcryptjs");

const auth = require("../auth");

// Add product

module.exports.addProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	console.log(userData);
	if(userData.isAdmin){
		return Product.find({productName: request.body.productName})
		.then(result =>{
			if(result.length > 0){
				return response.send(`${request.body.productName} is already listed. Add a different item.`);
			}
			else{
					let newProduct = new Product({
					productName: request.body.productName,
					description: request.body.description,
					price: request.body.price,
					stock: request.body.stock
				})
				newProduct.save()
				.then(product => response.send(`${newProduct.stock} ${newProduct.productName} are added to the stocks and selling for ${newProduct.price} per piece `))
				.catch(error => response.send("We encountered a problem processing the additional of product, please try again."))
			}
		})
		}
	else{
		response.send("You don't have acces to this page, go back to the previous page. Thank you!");
	}
};

// Retrieve all active products
module.exports.getAllActiveProducts = (request, response) => {
	return Product.find({isActive: true})
	.then(result => response.send(result));
};

//Retrieve single product
module.exports.getProduct = (request, response) => {
	return Product.findById(request.params.productId)
	.then(result => response.send(result));
};

// Update Product Information
module.exports.updateProduct = (request, response)=>{
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		let updateProduct = {
			productName: request.body.productName,
			description: request.body.description,
			price: request.body.price,
			stock: request.body.stock
			}
		
		return Product.findByIdAndUpdate(request.params.productId, updateProduct, {new:true})
		.then(update => response.send(update))
		.catch(error => response.send(false));
		}
		else{
			return response.status(401).send("You don't have acces to this page, go back to the previous page. Thank you!");
	}
		
};

	//Retrieve All Products

module.exports.viewProducts = (request, response) =>{
	const userData = auth.decode(request.headers.authorization);
	if(userData.isAdmin){
		return Product.find({}).then(result => response.send(result));
	}
	else{
		return response.status(401).send("You don't have acces to this page, go back to the previous page. Thank you!");
	}
};

//  Archive Product

module.exports.archiveProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	
	let updateIsActive = {
		isActive: request.body.isActive
	}
	if(userData.isAdmin){
		return Product.findByIdAndUpdate(request.params.productId, updateIsActive, {new:true})
		.then(result => response.send(result))
		.catch(error => {response.send(false)});
	}
	else{
		return response.send(401).send("You don't have acces to this page, go back to the previous page. Thank you!");
	}
}

