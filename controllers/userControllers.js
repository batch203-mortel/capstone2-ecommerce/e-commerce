const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcryptjs");
const auth = require("../auth");


// User registration
module.exports.userRegistration = (request,response)=>{
	return User.find({email: request.body.email})
	.then(result => {
		if(result.length > 0){
			return response.send(`${request.body.email} is already associated with other account, use another email and try again.`)
		}
		else{
			if(request.body.age <= 17){
				return response.send("You must be 18 years old or above before you can register.")
			}
			else{
				let newUser = new User({
					firstName: request.body.firstName,
					lastName: request.body.lastName,
					email: request.body.email,
					password: bcrypt.hashSync(request.body.password,10),
					age: request.body.age,
					mobileNumber: request.body.mobileNumber,
					deliveryAddress: request.body.deliveryAddress

				})
				return newUser.save()
				.then(user => {
					console.log(user);
					response.send("Successfully registered!")})
				.catch(error => {
					console.log(error);
					response.send("We're sorry, we had error during the process. Sorry for the inconvenience, please try again.");
				})
			}
		}
	})
}

// User Login
module.exports.loginUser = (request, response) =>{
	return User.findOne({email: request.body.email})
	.then(result=>{
		if(result === null){
			return response.send(`${request.body.email} is not associated in any account, register first.`)
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);
			if(isPasswordCorrect){
				return response.send({accessToken: auth.createAccessToken(result)})
			}
			else{
				return response.send("Incorrect password. Please try again.")
			}
		}
	})
}

// Get User detail

module.exports.getUser = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	/*console.log(userData);*/
	return User.findById(userData.id)
	.then(result => {
		result.password = "*****";
		response.send(result);
	})

}

// Non-admin User check-out
module.exports.checkout = async (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	console.log(userData);
	if(userData.isAdmin){
		return response.send("Admin accounts cannot place order!");
	}
	else{
		isUserUpdated = await User.findById(userData.id)
		.then(user => {user.orderedProducts.push({ totalAmount: request.body.totalAmount, product: request.body.product});
		return user.save().then(result=>{console.log(result);
			return true;
		})
		.catch(error => {
			console.log(error);
			return false;
		});});

		for(let i = 0; i < request.body.product.length; i++){
			let productName = await Product.findById(request.body.product[i].productId)
			.then(result => result.productName);

			let data = {
						userId: userData.id,
						email: userData.email,
						productId: request.body.product[i].productId,
						quantity: request.body.product[i].quantity,
						productName: productName

						};

			isProductUpdated = await Product.findById(data.productId)
			.then(product => {product.orderTransaction.push({
								userId: data.userId,
								email: data.email,
								productId: data.productId,
								productName: data.productName,
								quantity: data.quantity
								});

			product.stock -= data.quantity;
			return product.save()
			.then(result=>{
			console.log(result);
			return true
			})
			.catch(error => {
			console.log(error);
			return false;

			});
			
			});
			

}
	(isUserUpdated && isProductUpdated) ? response.send("Your product was successfully ordered. Thank you and please order again!") : response.send(false)
}
};


module.exports.updateRole = async(request, response) => {
	const userData = auth.decode(request.headers.authorization);
	if(userData.isAdmin){
		let isAdmin = await User.findById(request.params.id)
		.then(result => result.isAdmin)
		let firstName =await User.findById(request.params.id)
		.then(result => result.firstName)
		let lastName = await User.findById(request.params.id)
		.then(result => result.lastName)
		console.log(isAdmin);
		if(isAdmin){
			return response.send(`${firstName} ${lastName} has Admin roles already.`)
		}
		else{
			let updated = {
				isAdmin : true
				}

		return await User.findByIdAndUpdate(request.params.id, updated, {new: true})
		.then(updatedRole => response.send(`${updatedRole.firstName} ${updatedRole.lastName} is now set to Admin and has Admin Roles.`))
		.catch(error => response.send(error));
	}
		}
	else{
		return response.send("Only Admins are allowed to change role of the users!")
	}
			
	
	}

module.exports.getUserOrders = (request,response) => {
	const userData = auth.decode(request.headers.authorization);
	if(userData.isAdmin){
		return response.send('Admins are not allowed to checkout. They dont have order transaction');
	}
	else{
		return User.findById(userData.id)
		.then(result => response.send(result.orderedProducts))
	}

}

module.exports.retrieveAllOrders = async(request, response) => {
	const userData = auth.decode(request.headers.authorization);
	
	if(userData.isAdmin){
	let allTransactions = await User.find({isAdmin : false})
	.then(prods => prods);
	let lengths = allTransactions.length;
	let allOrders = []
	for(let i = 0; i < lengths ; i++ ){
		let orderedProducts = await User.findById(allTransactions[i].id)
		.then(result => result.orderedProducts)
		allOrders.push(orderedProducts);		
	}
	return response.send(allOrders)		
}
	else{
		return response.send('Offlimits, only Admins has access to this page.')
	}
}

