const mongoose =require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "Enter your First Name!"]
	},
	lastName:{
		type: String,
		required: [true, "Enter your Last Name!"]
	},
	email:{
		type: String,
		required: [true, "Enter your Last Name!"]
	},
	password:{
		type: String,
		required: [true, "Enter your desired Password!"]
	},
	age:{
		type: Number,
		required: [true, "Enter your age!"]
	},
	mobileNumber:{
		type: String,
		required: [true, "Enter your Mobile Number!"]
	},
	deliveryAddress:{
		type: String,
		required: [true, "Enter your delivery address!"]
	},
	isAdmin:{
		type: Boolean,
		default: false
	},
	dateCreated:{
		type: Date,
		default: new Date()
	},
	orderedProducts:[
		{
			totalAmount:{
				type:Number,
				required: [true, "Please add your total Amount!"]
			},
			dateAdded:{
				type: Date,
				default: new Date()
			},
			product: [
			{productID:{
				type: String,
				
			},
			productName:{
				type: String,
				
			},
			quantity:{
				type: Number,
				
			},
			status: {
				type: String,
				default: "Order was succesfully placed!"
			},
			isPaid:{
				type: Boolean,
				default: true
			}
		}]
			
		}

	]

})

module.exports = mongoose.model("User", userSchema);