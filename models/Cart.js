const mongoose =require("mongoose");

const cartSchema = new mongoose.Schema({
	firstName: {
		type: String
	},
	lastName: {
		type: String
	},
	email:{
		type: String
	},
	cart: [
		{
			totalAmount: {
				type: Number
			},
			datePlaced:{
				type: Date,
				default: new Date()
			},
			products:[{
				productId: {
					type: String
				},
				productName:{
					type: String
				},
				quantity:{
					type: Number
				}
			}

			]
		}


	]

})