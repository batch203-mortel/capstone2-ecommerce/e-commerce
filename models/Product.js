const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	productName: {
		type: String,
		required: [true, "Enter your Product Name!"]
	},
	description:{
		type: String,
		required: [true, "Enter product description!"]
	},
	price:{
		type: Number,
		required: [true, "Enter your product price!"]
	},
	stock:{
		type: Number,
		required: [true, "Enter the number of stocks available!"]
	},
	isActive:{
		type: Boolean,
		default: true
	},
	productAdded:{
		type: Date,
		default: new Date
	},
	orderTransaction:[
		{
			userid:{
				type: String,
				
			},
			email:{
				type: String,
				
			},
			quantity:{
				type:Number,
				
			},
			isPaid:{
				type: Boolean,
				defaults: true
			},
			dateOrdered:{
				type: Date,
				default: new Date()
			}

		}

	]
})

module.exports = mongoose.model("Product", productSchema);