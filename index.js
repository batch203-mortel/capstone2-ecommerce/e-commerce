const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const cartRoutes = require("./routes/cartRoutes");

const app = express();

// MongoDB connection

mongoose.connect("mongodb+srv://admin:admin@m0.wsmwmgh.mongodb.net/?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the cloud."))

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/carts", cartRoutes);

const port = process.env.Port || 4000;

app.listen(port, ()=>{
	console.log(`API is now live on port ${port}.`)
})

