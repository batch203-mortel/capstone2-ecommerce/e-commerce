const express = require("express");
const router = express.Router();
const auth = require("../auth");

const productControllers = require("../controllers/productControllers");

// Route for creating product
router.post("/", auth.verify, productControllers.addProduct);

// Route for retrieveng all courses(Admin)
router.get("/allProducts", auth.verify, productControllers.viewProducts);

// Route for viewing all active courses
router.get("/activeproducts", productControllers.getAllActiveProducts);

//Route for viewing specific product
router.get("/:productId", productControllers.getProduct);


// Route for Update for Product Information
router.put("/update/:productId", auth.verify, productControllers.updateProduct);

// Route for Archiving a product
router.patch("/:productId", auth.verify, productControllers.archiveProduct)




module.exports = router;