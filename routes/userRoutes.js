const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");
console.log(userControllers);
const auth = require("../auth");



// Route for User Registration
router.post("/register", userControllers.userRegistration);

// Route for User Login
router.post("/login", userControllers.loginUser);


// Route for checkout products
router.post("/checkout", auth.verify, userControllers.checkout);

// Change role
router.put("/updaterole/:id", auth.verify, userControllers.updateRole)


// Route for User detail
router.get("/details", auth.verify, userControllers.getUser);

// retrieve all orders
router.get("/allorders", auth.verify, userControllers.retrieveAllOrders);

// Route for user order
router.get("/orders", auth.verify, userControllers.getUserOrders);







module.exports = router;